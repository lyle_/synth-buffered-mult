# Multitudes

A simple 8hp, four-channel buffered multiple Eurorack module.
Outputs for unconnected inputs are chained to the connected input above, allowing more than 2 copies of a signal (up to 8) if there are unused inputs.

## Schematic

![schematic](images/schematic.png)

## Photos

![mult1](images/mult1.jpg)
![mult2](images/mult2.jpg)
![mult3](images/mult3.jpg)
